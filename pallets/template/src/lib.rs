#![cfg_attr(not(feature = "std"), no_std)]

/// Edit this file to define custom logic or remove it if it is not needed.
/// Learn more about FRAME and the core library of Substrate FRAME pallets:
/// <https://docs.substrate.io/reference/frame-pallets/>
pub use pallet::*;

#[frame_support::pallet]
pub mod pallet {
	use frame_support::{pallet_prelude::*, BoundedBTreeSet};
	use frame_system::pallet_prelude::*;

	use codec::alloc::vec::Vec;

	#[pallet::pallet]
	#[pallet::generate_store(pub(super) trait Store)]
	pub struct Pallet<T>(_);

	/// Configure the pallet by specifying the parameters and types on which it depends.
	#[pallet::config]
	pub trait Config: frame_system::Config {
		/// Because this pallet emits events, it depends on the runtime's definition of an event.
		type RuntimeEvent: From<Event<Self>> + IsType<<Self as frame_system::Config>::RuntimeEvent>;

		#[pallet::constant]
		/// Maximum length of the json representation of votes
		type MaxVoteJSONLength: Get<u32>;

		#[pallet::constant]
		/// Maximum length of an encrypted ballot
		type MaxEncryptedBallotLength: Get<u32>;

		#[pallet::constant]
		/// Maximum length of an encrypted Telling Token
		type MaxEncryptedTellingTokenLength: Get<u32>;

		#[pallet::constant]
		/// Maximum number of ballots for a vote
		type MaxBallots: Get<u32>;

		#[pallet::constant]
		/// Maximum number of nodes
		type MaxNodes: Get<u32>;
	}

	// Custom types abstractions
	type VoteJson<T> = BoundedVec<u8, <T as Config>::MaxVoteJSONLength>;
	type EncryptedBallot<T> = BoundedVec<u8, <T as Config>::MaxEncryptedBallotLength>;
	type EncryptedBallots<T> = BoundedBTreeSet<EncryptedBallot<T>, <T as Config>::MaxBallots>;
	type EncryptedTellingToken<T> = BoundedVec<u8, <T as Config>::MaxEncryptedTellingTokenLength>;
	type EncryptedTellingTokens<T> = BoundedBTreeSet<EncryptedTellingToken<T>, <T as Config>::MaxBallots>;
	type Validations<T> = BoundedBTreeSet<<T as frame_system::Config>::AccountId, <T as Config>::MaxNodes>;

	type VoteIndex = u32;

	#[derive(Clone, Encode, Decode, Eq, PartialEq, RuntimeDebug, Default, TypeInfo, MaxEncodedLen)]
	#[scale_info(skip_type_params(T))]
	pub struct VoteOnChainData<T: Config> {
		vote_json: VoteJson<T>,
		encrypted_ballots: EncryptedBallots<T>,
		encrypted_telling_tokens: EncryptedTellingTokens<T>,
		/// 0 prepared, 1 open, 2 closed, 3 validated, 4 canceled, 5 archived
		status: u8,
		validations: Validations<T>
	}

	// The pallet's runtime storage items.
	// https://docs.substrate.io/main-docs/build/runtime-storage/
	#[pallet::storage]
	#[pallet::getter(fn my_map)]
	pub type VotesMap<T: Config> = StorageMap<_, Twox256, VoteIndex, VoteOnChainData<T>>;

	// Pallets use events to inform users when important changes are made.
	// https://docs.substrate.io/main-docs/build/events-errors/
	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		/// Event documentation should end with an array that provides descriptive names for event
		/// parameters. [something, who]
		SomethingStored { something: u32, who: T::AccountId },
	}

	// Errors inform users that something went wrong.
	#[pallet::error]
	pub enum Error<T> {
		/// Something terrible happend
		Internal,
		/// Value bigger than storage bounds.
		StorageOverflow,
		/// Index not found, already used or out of range
		BadIndex,
		/// Duplicate vote or ballot
		Duplicate,
		/// Action violates protocol
		Protocol,
	}

	// Dispatchable functions allows users to interact with the pallet and invoke state changes.
	// These functions materialize as "extrinsics", which are often compared to transactions.
	// Dispatchable functions must be annotated with a weight and must return a DispatchResult.
	#[pallet::call]
	impl<T: Config> Pallet<T> {
		/// Adds a vote to the block chain in a JSON encoded and initiate the associated blockchain values
		#[pallet::call_index(0)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn add_vote(origin: OriginFor<T>, vote_index: VoteIndex, vote_json: Vec<u8>) -> DispatchResult {
			let _ = ensure_signed(origin).map_err(|_| Error::<T>::Internal)?;
 
			let vote_json: VoteJson<T> =
				vote_json.try_into().map_err(|_| Error::<T>::StorageOverflow)?;

			if let Ok(_) = <VotesMap<T>>::try_get(vote_index) {
				Err(Error::<T>::Duplicate.into())
			} else {
				let vote = VoteOnChainData {
					vote_json,
					encrypted_ballots: EncryptedBallots::<T>::default(),
					encrypted_telling_tokens: EncryptedTellingTokens::<T>::default(),
					status: 0,
					validations: Validations::<T>::default()
				};
				<VotesMap<T>>::insert(vote_index, vote);
				// TODO emit vote added (vote)
				Ok(())
			}
		}

		/// Update the status of a vote. Conditions apply :
		/// - it must be >= to the current one
		/// - Canceled (4) and Archived (5) statuses are definitive
		#[pallet::call_index(1)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn update_status(origin: OriginFor<T>, vote_index: VoteIndex, new_status: u8) -> DispatchResult {
			let _ = ensure_signed(origin).map_err(|_| Error::<T>::Internal)?;

			// Statuses bound are between Open (1) and Archived (5). Prepared (0) excluded as below
			if new_status <= 1 || new_status > 5 {
				return Err(Error::<T>::Protocol.into())
			}

			<VotesMap<T>>::mutate(vote_index, |vote| match vote {
				Some(vote) => {
					// Can't decrement status or change from Canceled (4) or Archived (5)
					if new_status <= vote.status || vote.status > 3 {
						return Err(Error::<T>::Protocol.into())
					}
		
					vote.status = new_status;
					// TODO emit status update(vote index, new status)
					Ok(())
				},
				None => Err(Error::<T>::BadIndex.into())
			})
		}

		#[pallet::call_index(2)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn add_encrypted_ballot(origin: OriginFor<T>, vote_index: VoteIndex, encrypted_ballot: Vec<u8>, encrypted_telling_token: Vec<u8>) -> DispatchResult {
			let _ = ensure_signed(origin).map_err(|_| Error::<T>::Internal)?;

			let encrypted_ballot: EncryptedBallot<T> =
				encrypted_ballot.try_into().map_err(|_| Error::<T>::StorageOverflow)?;

			let encrypted_telling_token: EncryptedTellingToken<T> =
				encrypted_telling_token.try_into().map_err(|_| Error::<T>::StorageOverflow)?;

			<VotesMap<T>>::mutate(vote_index, |vote| match vote {
				Some(vote) => {
					// Only accept ballots if the vote is open
					if vote.status != 1 {
						return Err(Error::<T>::Protocol.into())
					}

					let ballot_inserted = vote.encrypted_ballots.try_insert(encrypted_ballot).map_err(|_| Error::<T>::StorageOverflow)?;
					if ballot_inserted {
						let token_inserted = vote.encrypted_telling_tokens.try_insert(encrypted_telling_token).map_err(|_| Error::<T>::Internal)?;
						if !token_inserted {
							(||{})() // NOOP
							// TODO emit internal_error(consistency)
						}
						// TODO emit added ballot(vote_index, encrypted_telling_token)
						Ok(())
					} else {
						Err(Error::<T>::Duplicate.into())
					}
				},
				None => Err(Error::<T>::BadIndex.into())
			})
		}
	}
}
